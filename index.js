var dssv = [];
var BASE_URL = "https://62f8b755e0564480352bf411.mockapi.io";
function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}
function renderTable(list) {
  var contentHTML = "";
  list.forEach((item) => {
    var content = `
    <tr>
    <td>${item.ma}</td>
    <td>${item.ten}</td>
    <td>${item.email}</td>
    <td><img src="${item.hinhAnh}" style="width:40px" /></td>
    <td>
    <button onclick="xoaSV('${item.ma}')" type="button" class="btn btn-danger">Xóa</button>
    <button onclick="suaSV('${item.ma}')" type="button" class="btn btn-warning">Sửa</button>
    </td>
    </tr>
    `;
    contentHTML += content;
  });
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function renderDssv() {
  batLoading();
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (res) {
      renderTable(res.data);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}
renderDssv();

function xoaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      renderDssv();
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}
function themSV() {
  batLoading();
  var sv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      renderDssv();
      tatLoading();
    })
    .catch(function (err) {
      renderDssv();
      tatLoading();
    });
}

function suaSV(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/sv/${id}`,
    method: "GET",
  })
    .then(function (res) {
      showThongTinLenForm(res.data);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}

function capNhatSV() {
  batLoading();
  var dataSv = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/sv/${dataSv.ma}`,
    method: "PUT",
    data: dataSv,
  })
    .then(function (res) {
      renderDssv();
      tatLoading();
    })
    .catch(function (err) {
      renderDssv();
      tatLoading();
    });
}
