function layThongTinTuForm() {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var hinhAnh = document.getElementById("txtImg").value;
  var sv = {
    ma: ma,
    ten: ten,
    email: email,
    hinhAnh: hinhAnh,
  };
  return sv;
}
function showThongTinLenForm(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtImg").value = sv.hinhAnh;
}
